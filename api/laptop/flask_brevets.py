#!/usr/bin/env python
"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)
"""
import flask
from flask import Flask, redirect, url_for, request, render_template, flash, session
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from flask_restful import Resource, Api
import pymongo
from pymongo import MongoClient
from forms import LoginForm, RegisterForm
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
from testToken import generate_auth_token, verify_auth_token
from password import hash_password, verify_password
import os
import random
from urllib.parse import urlparse, urljoin
from flask_wtf.csrf import CSRFProtect
from flask_httpauth import HTTPBasicAuth
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)

# Globals
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)
csrf = CSRFProtect(app)

client = MongoClient('api_db_1', 27017)
db = client.tododb
u = db.udb
to_db = []

# Users===============
class User(UserMixin):
    def __init__(self, id, active=True):
        self.id = id
        self.active = active

# note that the ID returned must be unicode
# Login===============
login_manager = LoginManager()
login_manager.setup_app(app)
# step 6 in the slides
login_manager.login_view = u"login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = u"reauth"
login_manager.session_protection = u"strong"
# step 2 in slides
@login_manager.user_loader
def load_user(id):
    return u.find_one({"id": id})

# Pages===============
@app.route("/")
def calc():
    return render_template("calc.html")

@app.route('/api/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        id = random.randint(0, 9999999)
        password = hash_password(form.password.data)
        session['token'] = None

        next = request.args.get('next')
        if not is_safe_url(next):
            return flask.abort(400)

        if u.find_one({"username": form.username.data}) is None:
            u.insert_one({"id": id, "username": form.username.data, "password": password})
            return flask.jsonify({'name': [form.username.data,id]},
                           {'Location': url_for('get_user', id=id, _external=True)}), 201
        else:
            return render_template('login.html')
    return render_template('register.html',  title='Sign In', form=form)

@app.route('/api/login', methods=['GET', 'POST'])
def login():
    id = random.randint(0, 9999999)

    form = LoginForm()
    if form.validate_on_submit():
        user = User(id)
        session['id'] = id
        if u.find_one({"username": form.username.data}) == None:
            return redirect(url_for('register'))

        check_pass = u.find_one({"username": form.username.data})['password']
        if verify_password(form.password.data, check_pass):
            login_user(user, remember=form.remember_me.data)

        next = request.args.get('next')
        if not is_safe_url(next):
            return flask.abort(400)
        if next:
            print('t1============',next,flush=True)
            return redirect(next)
        return redirect(url_for('calc'))

    return render_template('login.html',  title='Sign In', form=form)

# step 5 in slides
@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        return redirect(request.args.get("next") or url_for("calc"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    session.pop('username', None)
    session.pop('password', None)
    session.pop('id', None)
    session.pop('token', None)
    flash("Logged out.")
    return redirect(url_for("login"))

@app.route("/api/token")
@login_required
def token():
    t = generate_auth_token(session['id'], 600)
    token = t['token'].decode('utf-8')
    session['token'] = token
    result = {'token': token, 'duration': 600}
    return flask.jsonify(result=result)

@app.route('/api/users')
def get_user():
    return {'name': str(session['user'])}

@app.route('/display')
@login_required
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) == 0:
            return render_template('empty.html')
    return render_template('display.html', items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return render_template('404.html'), 404

# AJAX request handlers===============
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    global to_db
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=float)
    date = request.args.get('open_date', type=str)
    time = request.args.get('open_time', type=str)
    index = request.args.get('index', type=int)
    date_time = date + ' ' + time
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    time = arrow.get(date_time, 'YYYY-MM-DD HH:mm').isoformat()
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)
    result = {"open": open_time, "close": close_time}
    db_entry = {"user": session['user'], "id": session['id'], "password": session['pass'],
                "index": index, "km": km, "distance": distance, "date": date,"time": time,
                 "open": open_time, "close": close_time}
    for line in to_db:
        if db_entry['index'] == line['index']:
            to_db.remove(line)
    to_db.append(db_entry)
    return flask.jsonify(result=result)

@app.route("/submit")
def submit():
    global to_db
    result = {"valid": True}
    if len(to_db) == 0:
        result['valid'] = False
    else:
        for line in to_db:
                db.tododb.insert_one(line)
        to_db = []
    return flask.jsonify(result=result)

#Resources===============
class all(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'open': [arrow.get(item['open']).format('YYYY-MM-DD HH:mm') for item in items],
            'close': [arrow.get(item['close']).format('YYYY-MM-DD HH:mm') for item in items]}

class open(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'open': [arrow.get(item['open']).format('YYYY-MM-DD HH:mm') for item in items]}

class close(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = request.args.get("top")
        if(top == None):
            top = 20
        _items = db.tododb.find().sort('close', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'close': [arrow.get(item['close']).format('YYYY-MM-DD HH:mm') for item in items]}

class all_json(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'open': [arrow.get(item['open']).format('YYYY-MM-DD HH:mm') for item in items],
            'close': [arrow.get(item['close']).format('YYYY-MM-DD HH:mm') for item in items]}

class open_json(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'open': [arrow.get(item['open']).format('YYYY-MM-DD HH:mm') for item in items]}

class close_json(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('close', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return {'close': [arrow.get(item['close']).format('YYYY-MM-DD HH:mm') for item in items]}

class all_csv(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        open = ''
        close = ''
        for item in items:
            open += arrow.get(item['open']).format('YYYY-MM-DD HH:mm') + ','
            close += arrow.get(item['close']).format('YYYY-MM-DD HH:mm') + ','
        return {'open': [open], 'close': [close]}

class open_csv(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('open', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        open = ''
        for item in items:
            open += arrow.get(item['open']).format('YYYY-MM-DD HH:mm') + ','
        return {'open': [open]}

class close_csv(Resource):
    def get(self):
        if check_token() == False:
            return "Token invalid or missing: route to /api/token after logging in to access resources", 401
        top = check_top()
        _items = db.tododb.find().sort('close', pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        close = ''
        for item in items:
            close += arrow.get(item['close']).format('YYYY-MM-DD HH:mm') + ','
        return {'close': [close]}

api.add_resource(all, '/listAll')
api.add_resource(open, '/listOpenOnly')
api.add_resource(close, '/listCloseOnly')
api.add_resource(all_json, '/listAll/json')
api.add_resource(open_json, '/listOpenOnly/json')
api.add_resource(close_json, '/listCloseOnly/json')
api.add_resource(all_csv, '/listAll/csv')
api.add_resource(open_csv, '/listOpenOnly/csv')
api.add_resource(close_csv, '/listCloseOnly/csv')

#helper functions=========================
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def check_top():
    top = request.args.get("top")
    if(top == None):
        top = 20
    return top

def check_token():
    if "token" in session:
        if session['token'] == None:
            token = request.args.get('token')
        else:
            token = session['token']
    else:
        token = request.args.get('token')
    if token == None:
        return False
    elif verify_auth_token(token) == None:
        return False
    else:
        return True

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
    csrf.init_app(app)
