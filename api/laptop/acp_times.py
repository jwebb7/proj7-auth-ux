"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/pages/rulesForRiders
"""
import arrow

"""
online resources used:
https://arrow.readthedocs.io/en/latest/
https://stackoverflow.com/questions/1085801/get-selected-value-in-dropdown-list-using-javascript
https://serverfault.com/questions/683605/docker-container-time-timezone-will-not-reflect-changes
"""
brevets = []
# (brevet, min, max)
brevets.append((200, 15, 34))
brevets.append((400, 15, 32))
brevets.append((600, 15, 30))
brevets.append((1000, 11.428, 28))

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #check for 20% overlap
    if control_dist_km < (brevet_dist_km + (.2 * brevet_dist_km)) and control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km

    brevet_time = 0
    big_control = control_dist_km

    for brevet in brevets:
        if control_dist_km > brevet[0]:
            brevet_time += (200 / brevet[2])
            big_control -= 200
        else:
            if big_control == control_dist_km:
                brevet_time = control_dist_km / brevet[2]
                break
            else:
                brevet_time += (big_control) / brevet[2]
                break

    split_time = str(brevet_time).split(".")
    hour = int(split_time[0])
    minute = int(round(float('.' + split_time[1]) * 60.0))
    #add time from calculation to start time
    return arrow.get(brevet_start_time).shift(hours=+hour, minutes=+minute).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    limits = [] #article 9 in rules for riders
    limits.append((200, '13:30'))
    limits.append((300, '20:00'))
    limits.append((400, '27:00'))
    limits.append((600, '40:00'))
    limits.append((1000, '75:00'))

    if control_dist_km == 0: #minimum closing time
        return arrow.get(brevet_start_time).shift(hours=+1).isoformat()

    #check for 20% overlap and maximum
    if control_dist_km < (brevet_dist_km + (.2 * brevet_dist_km)) and control_dist_km >= brevet_dist_km:
        control_dist_km = brevet_dist_km
        for limit in limits:
            if control_dist_km == limit[0]:
                time = limit[1].split(':')
                return arrow.get(brevet_start_time).shift(hours=+int(time[0]), minutes=+int(time[1])).isoformat()

    brevet_time = 0
    big_control = control_dist_km

    for brevet in brevets:
        if control_dist_km > brevet[0]:
            brevet_time += (200 / brevet[1])
            big_control -= 200
        else:
            if big_control == control_dist_km:
                brevet_time = control_dist_km / brevet[1]
                break
            else:
                brevet_time += (big_control) / brevet[1]
                break

    split_time = str(brevet_time).split(".")
    hour = int(split_time[0])
    minute = int(round(float('.' + split_time[1]) * 60.0))
    #add time from calculation to start time
    return arrow.get(brevet_start_time).shift(hours=+hour, minutes=+minute).isoformat()
